# An iOS uiWebView and wkWebView Javascript Bridge #

This repository contains an iOS bridge for communicating between native iOS and xWebView--hosted Javascript. Some of the features of this bridge include:

* Written in Swift
* Detects iOS8 or greater and uses newer, [faster](http://developer.telerik.com/featured/why-ios-8s-wkwebview-is-a-big-deal-for-hybrid-development/) WkWebView. Falls back to UiWebView for iOS7. Prior to iOS7 is not supported (because Swift is not supported prior to iOS7).
* Can load app-bundled and network-based HTML content.
* Can control how it caches network-based HTML content for offline use.
* Facilitates calls from native to xWebView-hosted JavaScript and return of values.
* Facilitates calls from xWebView-hosted JavaScript to native and return of values.
* JavaScript library is compatible with [Android bridge](https://bitbucket.org/rmorley/androidwebviewbridgefragment) and uses the same [JavaScript bridge](https://bitbucket.org/rmorley/androidwebviewbridgefragment/src/8fc6e581777b8c447e2551aea88aef7f6870535d/js/bridge.js?at=master).

### Setup ###
1. Add this project to your app project. For apps intended to be iOS7 compatible and for app store submission, copy source files from this project into your app project instead (see [Embedded frameworks only supported in iOS8](http://stackoverflow.com/a/25910262))
2. Derive a ViewController off of WebBridgeViewController, overriding either  **getLoadConfiguration()** or **getLoadUrl()** (see below)
3. For app bundled HTML content, create and put files in a project '**www**' folder.
4. Include [bridge.js](https://bitbucket.org/rmorley/androidwebviewbridgefragment/src/8fc6e581777b8c447e2551aea88aef7f6870535d/js/bridge.js?at=master) in your JavaScript and reference in your HTML file. 


Example of a ViewController derived off of WebBridgeViewController.

```
#!swift
import UIKit
import WebBridge

class WebViewController : WebBridgeViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

/*    internal override func getLoadConfiguration() -> (prot : String, host : String, path : String?, query : String?, loadMode : LoadMode)? {
        return ("http", "www.cnn.com", nil, nil, LoadMode.LoadNormal)
    }
*/   
    internal override func getLoadUrl() -> NSURL? {
        if let path = NSBundle.mainBundle().pathForResource("test", ofType:"html", inDirectory:"www") {
            return NSURL.fileURLWithPath(path)
        } else {
            return nil;
        }
    }

}


```
### Use ###

* From your ViewController, make calls into JavaScript using the following inherited method:
```
#!swift

callWebviewJs(functionStatement: String, handleReturnFromJs: ((String?, NSError?) -> Void)?)
```

* From Javascript, call into iOS by invoking: 
```
#!JavaScript

Bridge.exec(successHandlerCallback, errorHandlerCallback, "SERVICENAME", "ACTIONNAME", ["PARAM1", "PARAM2", ... ]);
```

* Javascript calls into iOS result in invocation of the following method, which a derived ViewController should implement:
```
#!swift

callNative(service : String, action : String, params : [String]?, handleReturnFromNative : ((returnValue: String, isError: Bool)->Void)? )
```


### References ###
* [Swift compatibililty with different iOS versions](https://developer.apple.com/swift/blog/?id=2)
* [Adding Swift framework to Swift app](http://stackoverflow.com/questions/25909870/xcode-6-and-embedded-frameworks-only-supported-in-ios8)
* Swift framework issues: [Cocoa Touch and Static Library](http://stackoverflow.com/questions/24070726/building-pure-swift-cocoa-touch-framework) [Embedded framworks vs. iOS version](http://stackoverflow.com/questions/25909870/xcode-6-and-embedded-frameworks-only-supported-in-ios8)