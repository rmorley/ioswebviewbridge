//
//  Logger.swift
//  WebBridge
//
// see https://developer.apple.com/swift/blog/?id=15  "these identifiers (__FUNCTION__, etc.) expand to the location of the 
//   caller when evaluated in a default argument list."
//
//  Created by me on 12/16/14.
//  Copyright (c) 2014 Compass Point, Inc. All rights reserved.
//

import Foundation

public enum LogLevel : String {
    case Info = "INFO"
    case Error = "ERROR"
    case Bug = "BUG"
    case Trace = "TRACE"
}


public func log(logMessage: String, logLevel: LogLevel = LogLevel.Info, fileName: String = __FILE__, functionName: String = __FUNCTION__, lineNumber: Int = __LINE__) {
    #if DEBUG
        println("\(logLevel.rawValue): File: \(fileName), Function: \(functionName), Line: \(lineNumber), Message: \(logMessage)")
    #endif
}
