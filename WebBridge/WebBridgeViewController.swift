//
//  WebViewController.swift
//  WebBridge
//
//  Created by me on 12/4/14.
//  Copyright (c) 2014 Compass Point, Inc. All rights reserved.
//

import UIKit
import WebKit

public enum LoadMode {
    case LoadNormal
    case LoadCacheFirstWhenNoInet
}


public class WebBridgeViewController: UIViewController, UIWebViewDelegate, WKScriptMessageHandler, WKNavigationDelegate {

    let callbackHandlerName = "callNative"
    let wkWebViewClassName = "WKWebView"
    let processMessagesUrlScheme = "processrequestsfromjs"
    let processMessagesJavascriptGet = "Bridge.getPendingJsRequest()"
    let processMessagesJavascriptGetEndstring = ""

    var loadIdentifier : String?
    var loadUrl : NSURL?
    var loadMode : LoadMode = LoadMode.LoadNormal

    var wkWebView : WKWebView?
    var uiWebView : UIWebView?
    
    @IBOutlet public var containerView : UIView! = nil

    //Derived class overrides
    
    // Override either this method or getLoadUrl(). Framework tries this one first, then getLoadUrl()
    public func getLoadConfiguration() -> (prot : String, host : String, path : String?, query : String?, loadMode : LoadMode)? {
        return nil
    }
    
    // Override either this method or getLoadConfiguration(). Framework tries getLoadConfiguration() first, then getLoadUrl()
    public func getLoadUrl() -> NSURL? {
        return nil
    }
    
    public func getOpenInSafariUrlPrefixes() -> [String]? {
        return nil
    }

    public func getOpenInSameWebviewUrlPrefixes() -> [String]? {
        return nil
    }
    
    public func doLoad(reloadLatestGetUrlIfAvail : Bool) {

        if let loadConfiguration = self.getLoadConfiguration() {
            self.loadMode = loadConfiguration.loadMode
            if loadConfiguration.path != nil {
                self.loadIdentifier = "\(loadConfiguration.host)\(loadConfiguration.path!)"
            } else {
                self.loadIdentifier = "\(loadConfiguration.host)/"
            }
            var urlString : String
            if loadConfiguration.query != nil {
                urlString = "\(loadConfiguration.prot)://\(self.loadIdentifier!)?\(loadConfiguration.query)"
            } else {
                urlString = "\(loadConfiguration.prot)://\(self.loadIdentifier!)"
            }
            self.loadUrl = NSURL(string: urlString)
            log("Setting load url to \(urlString)")
        } else if let loadUrl = self.getLoadUrl() {
            self.loadUrl = loadUrl
            self.loadIdentifier = loadUrl.path
        }
        
        //TO DEAL WITH WKWebView bug where it can't load local files from bundle.
        if (self.loadUrl?.scheme == "file") && (NSClassFromString(wkWebViewClassName) != nil) {
            var error : NSError?
            if let newUrl = self.copyBundleFolderToTemp("www", mainFileInBundle: "test.html", error: &error) {
                self.loadUrl = newUrl
                self.loadIdentifier = loadUrl?.path
            } else {
                log("Error copying bundle directory. Error reports: \(error?.description)", logLevel: LogLevel.Bug)
            }
        }
        
        if loadUrl != nil {
            var request : NSURLRequest?
            if self.loadMode == LoadMode.LoadCacheFirstWhenNoInet {
                request = NSURLRequest(URL:loadUrl!, cachePolicy:/*NSURLRequestReturnCacheDataDontLoad*/NSURLRequestCachePolicy.ReturnCacheDataElseLoad,
                timeoutInterval:20.0)
                
                let reachability : Reachability = Reachability.reachabilityForInternetConnection()
                let status : NetworkStatus = reachability.currentReachabilityStatus()
                
                switch(status.value) {
                    case NotReachable.value:
                        log("Internet NOT available. Using cache.")
                    /*
                    case ReachableViaWiFi:
                    case ReachableViaWWAN:
                    */
                    default:
                        NSURLCache.sharedURLCache().removeAllCachedResponses()
                        log("Internet available. Deleting cache entry so it will reload.")
                }
            } else if (self.loadMode == LoadMode.LoadNormal) {
                request = NSURLRequest(URL:loadUrl!)
            }
            if NSClassFromString(wkWebViewClassName)  != nil { //if test for self.wkWebView != nil when in < ios8, BAD_ACCESS probably because library definition is not available.
                if self.wkWebView?.loadRequest(request!) == nil {
                    log("class WkWebView is found but self.wkWebView is not set.", logLevel: LogLevel.Bug)
                }
            } else if self.uiWebView != nil {
                self.uiWebView!.loadRequest(request!)
            } else {
                log("Neither wkWebView or uiWebView are set", logLevel: LogLevel.Bug)
            }
        } else {
            log("loadUrl is nil", logLevel: LogLevel.Error)
        }
    }

    
    override public func loadView() {
        super.loadView()
        
        if NSClassFromString(wkWebViewClassName)  != nil {
            var contentController = WKUserContentController();
            contentController.addScriptMessageHandler(
                self,
                name: callbackHandlerName
            )
            
            var config = WKWebViewConfiguration()
            config.userContentController = contentController
            
            self.wkWebView = WKWebView(
                frame: self.containerView.bounds,
                configuration: config
            )
            if self.wkWebView != nil {
                self.wkWebView!.navigationDelegate = self
                self.view.addSubview(self.wkWebView!)
 //               self.view = self.wkWebView!
                log("Set WkWebView as view", logLevel: LogLevel.Trace)
            } else {
                log("WKWebView available but wkWebView member not set", logLevel: LogLevel.Bug)
            }
        } else {
            self.uiWebView = UIWebView(frame:self.containerView.bounds)
            if (self.uiWebView?.delegate = self) == nil {
                log("uiWebView available but uiWebView member not set", logLevel: LogLevel.Bug)
            } else {
                log("Set UIWebView as view", logLevel: LogLevel.Trace)
                self.view.addSubview(self.uiWebView!)
//                self.view = self.uiWebView!
            }
        }
    }

    override public func viewDidLoad() {
        super.viewDidLoad()
 
        self.doLoad(false)
    }

    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    public func callWebviewJs(functionStatement: String, handleReturnFromJs: ((String?, NSError?) -> Void)?) {
        if NSClassFromString(wkWebViewClassName)  != nil {
            var completionHandlerOptional : ((AnyObject!, NSError!) -> Void)? = nil
            if handleReturnFromJs != nil {
                completionHandlerOptional = {(object: AnyObject!, error: NSError!) -> Void in
                    var returnString : String? = object as? String
                    handleReturnFromJs!(returnString, error)
                }
            }
 //           var handleReturnFromJsFunc : (String?, NSError?) -> Void
            if self.wkWebView?.evaluateJavaScript(functionStatement, completionHandler: completionHandlerOptional) == nil {
                log("class WkWebView is found but self.wkWebView is not set.", logLevel: LogLevel.Bug)
            }
        } else if self.uiWebView != nil {
            var returnValue = self.uiWebView!.stringByEvaluatingJavaScriptFromString(functionStatement)
            if handleReturnFromJs != nil {
                var returnError : NSError?
                if returnValue == nil {
                    returnError = NSError(domain: "WebBridgeViewController", code: 44, userInfo: ["message" : "nil returned from UIWebView"])
                }
                dispatch_async(dispatch_get_main_queue(), {() -> Void in
                    handleReturnFromJs!(returnValue, returnError)
                })
            }
        } else {
            log("Neither wkWebView or uiWebView are set", logLevel: LogLevel.Bug)
        }
    }
    
    func callNative(requestJson : String) {
        var parseError: NSError?
        if let data = requestJson.dataUsingEncoding(NSUTF16StringEncoding) {
            if let messageDictionary = (NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: &parseError) as? Dictionary<String, AnyObject>) {
                if messageDictionary.isEmpty {
                    log("Dictionary from converted json string obtained from javascript is empty. Error reports " + parseError!.description, logLevel: LogLevel.Error)
                } else {
                    let requestId = messageDictionary["requestId"] as Int
                    let service = messageDictionary["service"] as String?
                    let action = messageDictionary["action"]  as String?
                    let params = messageDictionary["params"]  as [String]?
                    if (service != nil) && (action != nil) {
                        let handleReturnFromNative = {(returnValue: String, isError: Bool) -> Void in
                            var r = requestId
                        }
                        callNative(service!, action: action!, params: params, handleReturnFromNative)
                    } else {
                        log("action and/or service is not included in call from JavaScript.", logLevel: LogLevel.Error)
                    }
                }
            } else {
                log("Can't parse json string obtained from javascript. Error reports " + parseError!.description, logLevel: LogLevel.Error)
            }
        } else {
            log("Can't apply UTF8 encoding to string obtained from javascript", logLevel: LogLevel.Bug)
        }
    }
    
    public func callNative(service : String, action : String, params : [String]?, handleReturnFromNative : ((returnValue: String, isError: Bool)->Void)? ) {

        handleReturnFromNative!(returnValue: "returnValue", isError:false)
    }


    func okToNavigate(request : NSURLRequest) -> Bool {
        var url = request.URL

        if url.absoluteString != nil {
            log("url.aboluteString = " + url.absoluteString!, logLevel: LogLevel.Trace)
        } else {
            log("url.aboluteString is nil", logLevel: LogLevel.Error)
            return false;
        }
        
        if url.scheme != nil {
            log("url.scheme = " + url.scheme!, logLevel: LogLevel.Trace)
        } else {
            log("url.scheme is nil", logLevel: LogLevel.Trace)
        }
        
        if url.host != nil {
            log("url.host = " + url.host!, logLevel: LogLevel.Trace)
        } else {
            log("url.host is nil", logLevel: LogLevel.Trace)
        }
        
        if url.path != nil {
            log("url.path = " + url.path!, logLevel: LogLevel.Trace)
        } else {
            log("url.path is nil", logLevel: LogLevel.Trace)
        }
        
        var loadIdentifierToTest : String?
        
        if url.scheme == "file" {
//                assert(NSClassFromString(wkWebViewClassName) == nil, "WkWebViewClassName cannot process file:// urls at this time due to an Apple bug. See http://stackoverflow.com/questions/24882834/wkwebview-not-working-in-ios-8-beta-4 and http://trac.webkit.org/changeset/174029 . New WKWebView should have method -[WKWebView loadFileURL:allowingReadAccessToURL:]): once fixed.")
            if url.path != nil {
                loadIdentifierToTest = url.path!
            } else {
                return false  //file types must have paths.
            }
        } else {
            if (url.host != nil) && (url.path != nil) {
                loadIdentifierToTest = "\(url.host!)\(url.path!)"
            } else if (url.host != nil) {
                loadIdentifierToTest = "\(url.host!)/"
            } else {
                //return false
            }
        }
        if loadIdentifierToTest != nil {
            log("loadIdentifierToTest == " + loadIdentifierToTest!, logLevel: LogLevel.Trace)
            if loadIdentifierToTest == self.loadIdentifier {
                return true
            } else {
                if let openInSafariUrlPrefixes = getOpenInSafariUrlPrefixes() {
                    for urlPrefix in openInSafariUrlPrefixes {
                        if (url.absoluteString?.hasPrefix(urlPrefix) != nil) {
                            UIApplication.sharedApplication().openURL(url)
                            return false
                        }
                    }
                }
                if let openInSameWebviewUrlPrefixes = getOpenInSameWebviewUrlPrefixes() {
                    for urlPrefix in openInSameWebviewUrlPrefixes {
                        if (url.absoluteString?.hasPrefix(urlPrefix) != nil) {
                            return true;
                        }
                    }
                }
                return false
            }
        } else {
            log("loadIdentifierToTest == nil", logLevel: LogLevel.Trace)
            return false;
        }
     }
    
    //WKNavigationDelegate
    
    
     public func webView(webView: WKWebView,
        decidePolicyForNavigationAction navigationAction: WKNavigationAction,
        decisionHandler: (WKNavigationActionPolicy) -> Void) {
            var request : NSURLRequest = navigationAction.request
            var okToNavigate = self.okToNavigate(request)
            var navPolicy : WKNavigationActionPolicy
            if okToNavigate {
                navPolicy = WKNavigationActionPolicy.Allow
            } else {
                navPolicy = WKNavigationActionPolicy.Cancel
            }
            decisionHandler(navPolicy)
    }
    
    //UIWebViewDelegate
    
    public func webView(webView: UIWebView,
        shouldStartLoadWithRequest request: NSURLRequest,
        navigationType: UIWebViewNavigationType) -> Bool {
            if request.URL.scheme == processMessagesUrlScheme {// url.absoluteString!.hasPrefix(processMessagesUrlPrefix) {
                self.processMessages()
                return false
            } else {
                return okToNavigate(request)
            }
    }
    
    public func webViewDidStartLoad(webView: UIWebView) {
        
        
    }
    
    
    public func webViewDidFinishLoad(webView: UIWebView) {
        
    }
    
    public func webView(webView: UIWebView,
        didFailLoadWithError error: NSError) {
            
    }
    
    func processMessages() {
        while true {
            if let requestJson = self.uiWebView?.stringByEvaluatingJavaScriptFromString(processMessagesJavascriptGet) {
                if requestJson == processMessagesJavascriptGetEndstring {
                    break
                } else {
                    callNative(requestJson)
                 }
            } else {
                log("uiWebView is nil", logLevel: LogLevel.Bug)
                break;
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
 
    //WKScriptMessageHandler
    
    public func userContentController(userContentController: WKUserContentController, didReceiveScriptMessage message: WKScriptMessage) {
        if(message.name == callbackHandlerName) {
            if let requestJson = message.body as? NSString {
                println("JavaScript is sending a message through UIScriptMessageHandler \(requestJson)")
                self.callNative(requestJson)
            } else {
                log("JavaScript is sending a message through UIScriptMessageHandler but it isn't a dictionary.", logLevel: LogLevel.Bug)
            }
        }
    }
    
    //Copy bundle www files to tmp directory on iOS8 to deal with bug loading local files in WkWebView
    //http://stackoverflow.com/a/26054170
    func copyBundleFolderToTemp(bundleFolderName : String, mainFileInBundle : String, error : NSErrorPointer) -> NSURL? {
        //Get the path in the bundle of the bundleFolderName
        var path = NSBundle.mainBundle().pathForResource(mainFileInBundle, ofType: "", inDirectory: bundleFolderName)
        var pathToBundleFolder : String
        if path != nil {
            pathToBundleFolder = path!.stringByDeletingLastPathComponent
        } else {
            var errorString = "\(mainFileInBundle) is not in bundle directory: \(bundleFolderName)."
            if error != nil {
                error.memory = NSError(domain: "cpdomain", code:1, userInfo : [NSLocalizedDescriptionKey: errorString])
            }
            return nil;
        }

        //remove bundleFolderName and its contents in tmp directory if it exists because copyItemAtPath wants to create it.
        var dstDir : String = NSTemporaryDirectory().stringByAppendingPathComponent(bundleFolderName)
        if NSFileManager.defaultManager().fileExistsAtPath(dstDir) {
            if !NSFileManager.defaultManager().removeItemAtPath(dstDir, error: error) {
                return nil;
            }
        }
        
        //Copy items from bundle's folder to tmp directory folder
        if NSFileManager.defaultManager().copyItemAtPath(pathToBundleFolder, toPath: dstDir, error: error) {
            var newPath = dstDir.stringByAppendingPathComponent(mainFileInBundle)
            if let newUrl = NSURL.fileURLWithPath(newPath) {
                return newUrl
            } else {
                var errorString = "Could not obtain a newUrl from \(newPath)"
                if error != nil {
                    error.memory = NSError(domain: "cpdomain", code:3, userInfo : [NSLocalizedDescriptionKey: errorString])
                }
                return nil;
            }
        } else {
            return nil
        }
    }

}
